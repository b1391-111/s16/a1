let numberPrompt = prompt("Give me a number [50 and above]: ");
if(numberPrompt < 50) {
    alert("The input must be above 50. Terminating the loop");
} else {
    for(let i = +numberPrompt; i > 0; i--){
        if(i % 10 === 0){
            console.log(`${i} will be skipped`);
            continue;
        }
    
        if(i % 5 === 0){
            console.log(i);
        }
    }
}

const stringPrompt = "superfragilisticexpialidocious";
const string = stringPrompt.toLocaleLowerCase();
let result = [];

for(let i = 0; i < string.length; i++){
    if(string[i] === "a" || string[i] === "e" || string[i] === "i" || string[i] === "o" || string[i] === "u"){
        continue;
    } else {
        result.push(string[i]);
    }
}
console.log(stringPrompt);
console.log(result.join(""));